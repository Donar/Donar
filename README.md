# Donar

Donar enables anonymous VoIP with good quality-of-experience (QoE) over the Tor network.
No individual Tor link can match VoIP networking requirements. Donar bridges this gap by spreading VoIP traffic over several links.
